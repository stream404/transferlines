class HoteleModel {
    constructor(
        public name: string,
        public hotelID: number,
        public image: string,
        public location: string,
        public _id?: string, // _id is present if editing or returning from DB
    ) { }
}

class FormHoteleModel {
    constructor(
        public name: string,
        public location: string
    ) { }
}

export { HoteleModel, FormHoteleModel};
