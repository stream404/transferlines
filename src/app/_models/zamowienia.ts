class ZamowieniaModel {

    constructor (
        public type: string ,
        public state: string ,
        public date_add: Date ,
        public date_lastChange: Date ,
        public event_date: Date ,
        public event_time: Date ,
        public location_IN: string ,
        public location_OUT: string ,
        public distance: number ,
        public event_user: string ,
        public event_registred_name: string ,
        public event_registred_lastName: string ,
        public event_registred_payStatus: string ,
        public _id?: string
    ) {}

}

export { ZamowieniaModel };



