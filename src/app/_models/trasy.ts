class TrasyModel {
  constructor(
    public title: string,
    public locationIN: string,
    public locationOUT: string,
    public image: string,
    public startDatetime: Date,
    public endDatetime: Date,
    public description: string,
    public viewPublic: boolean,
    public defTime: string,
    public _id?: string, // _id is present if editing or returning from DB
  ) { }
}

class FormTrasyModel {
  constructor(
    public title: string,
    public locationIN: string,
    public locationOUT: string,
    public image: string,
    public startDate: string,
    public startTime: string,
    public endDate: string,
    public endTime: string,
    public viewPublic: boolean,
    public description: string,
    public defTime: string,
  ) { }
}

export { TrasyModel, FormTrasyModel};
