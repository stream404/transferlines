﻿export class User {
    _id: string;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    valid: string;
    role: string;
    email: string;
    hash: string;
    hotelID: number;
    verifiedBy: string;
    image: string;
}
