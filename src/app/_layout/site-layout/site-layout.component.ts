import { Component , OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component ( {
                 selector: 'app-site-layout' ,
                 templateUrl: './site-layout.component.html' ,
                 styleUrls: [ './site-layout.component.css' ] ,
                 providers: [ NgForm ]
             } )
export class SiteLayoutComponent implements OnInit {

    constructor () {

    }

    ngOnInit () {
    }

}

