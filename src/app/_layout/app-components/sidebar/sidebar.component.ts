import { optimizeGroupPlayer } from '@angular/animations/browser/src/render/shared';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../_services';
import { User } from '../../../_models';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;

}
export const ROUTES: RouteInfo[] = [
    {path: 'login', title: 'logowanie', icon: 'person', class:  ''},
    { path: 'users', title: 'Lista uzytkownikow',  icon:    'content_paste', class: '' },
    // { path: 'login', title: 'Dashboard',  icon: 'dashboard', class: '' },
    { path: 'user-profile', title: 'Konto',  icon:   'person', class: '' },
    { path: 'ad-trasfer-lot', title: 'Dodaj trasę',  icon:   'person', class: '' },
    { path: 'ad-wycieczki', title: 'Table List',  icon: 'content_paste', class: '' },

    // { path: 'table-list', title: 'Table List',  icon:'content_paste', class: '' },
    // { path: 'typography', title: 'Typography',  icon:'library_books', class: '' },
    // { path: 'icons', title: 'Icons',  icon:'bubble_chart', class: '' },
    // { path: 'maps', title: 'Maps',  icon:'location_on', class: '' },
    // { path: 'notifications', title: 'Notifications',  icon:'notifications', class: '' }
    ];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  public currentUser = '';
  constructor(public userService: UserService) {
  }

  ngOnInit() {
      this.currentUser = this.userService.currentUser();
      this.menuItems = ROUTES.filter(menuItem => menuItem);
console.log(this.currentUser, 'currentUser');
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
