import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AlertService } from '../_services';

@NgModule ( {
                imports: [
                    CommonModule ,
                    FormsModule
                ] ,
                declarations: [
                ] ,
                providers: [ AlertService ] ,
                exports: []
            } )
export class ClientModule {}
