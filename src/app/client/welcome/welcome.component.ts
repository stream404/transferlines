import { Component , OnInit } from '@angular/core';
import { ActivatedRoute , Router } from '@angular/router';
import { AlertService , AuthenticationService } from '../../_services';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';


declare var jquery: any;
declare var $: any;

@Component ( {
                 selector: 'app-welcome' ,
                 templateUrl: './welcome.component.html' ,
                 styleUrls: [ './welcome.component.scss' ] ,
                 providers: [ FormsModule , ReactiveFormsModule ]

             } )

export class WelcomeComponent implements OnInit {


    error = false;
    model: any = {};
    loading = false;
    returnUrl: string;

    constructor (
        private route: ActivatedRoute ,
        private router: Router ,
        private authenticationService: AuthenticationService ,
        private alertService: AlertService
    ) { }

    ngOnInit () {
        this.authenticationService.logout ();
        this.returnUrl = this.route.snapshot.queryParams[ 'returnUrl' ] || '/app/dashboard';


        const showMenu = function () {
            $.each ( $ ( ' .slide ' ) , function ( i , el ) {
                setTimeout ( function () {
                    $ ( el ).addClass ( 'in' );

                    $ ( el )
                        .find ( 'a' )
                        .addClass ( 'visible' );
                } , 200 + i * 200 );
            } );
        };

        setTimeout ( function () {
            showMenu ();
        } , 500 );
    }


    loginForm ( form ) {


        console.log ( form );
        this.alertService.modal ( 'Wysyłam' , false );
        this.loading = true;
        this.authenticationService.login ( form.username , form.password )
            .subscribe (
                data => {
                    this.router.navigate ( [ this.returnUrl ] );
                } ,
                error => {
                    this.alertService.error ( error );
                    this.loading = false;
                    this.error = true

                }
            );
    }
}
