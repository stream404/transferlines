import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders , HttpErrorResponse } from '@angular/common/http';
// import {AuthService} from './auth/auth.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { ZamowieniaModel } from '../_models/zamowienia';

import { appConfig } from '../app.config';
import { User } from '../_models/index';
import { HoteleModel } from '../_models/hotele';

@Injectable ()
export class ApiService {
    // public _handleError: TrasyModel[];
    constructor ( private http: HttpClient ,
                  // private auth: AuthService
    ) { }

    private get _authHeader (): string {
        return `Bearer ${localStorage.getItem ( 'access_token' )}`;
    }

    //
    // UPLOAD AVATAR TO server/uploads
    //
    public postFile ( fileToUpload: File , username: string ): Observable<boolean> {
        console.log ( fileToUpload.name );
        const endpoint = appConfig.apiUrl + '/api/upload_ua/';
        const formData: FormData = new FormData ();
        formData.append ( 'fileKey' , fileToUpload , fileToUpload.name );
        formData.set ( 'username' , username );
        return this.http
            .post ( endpoint , formData , { headers: new HttpHeaders ().set ( 'Authorization' , this._authHeader ) } )
            .map ( () => { return true; } )
            .catch ( this._handleError );
    }

    // UPLOAD AVATAR HOTEL TO server/uploads
    //
    public postAvatarHotel ( fileToUpload: File , hotelID: string ): Observable<boolean> {

        const endpoint = appConfig.apiUrl + '/api/upload_ha/';
        const formData: FormData = new FormData ();
        formData.append ( 'fileKey' , fileToUpload , fileToUpload.name );
        formData.set ( 'hotelID' , hotelID );
        return this.http
            .post ( endpoint , formData , { headers: new HttpHeaders ().set ( 'Authorization' , this._authHeader ) } )
            .map ( () => { return true; } )
            .catch ( this._handleError );
    }


    // GET all hotele (admin only)
    public get_Hotele (): Observable<any> {
        return this.http
            .get ( appConfig.apiUrl + '/api/hotele' )
            .catch ( this._handleError );
    }

    // GET all hotele by Admin (admin only)
    public get_AdminsHotele ( _id_admina ): Observable<any> {
        return this.http
            .get ( appConfig.apiUrl + '/api/hotelebyAdminId' + _id_admina , _id_admina )
            .catch ( this._handleError );
    }

    // SAVE hotel INFO (admin only)
    public save_Hotel ( hotelInfo: HoteleModel ): Observable<any> {
        return this.http
            .post ( appConfig.apiUrl + '/api/hotelsave' , hotelInfo , {
                headers: new HttpHeaders ().set ( 'Authorization' , this._authHeader )
            } )
            .catch ( this._handleError );
    }

    // DEL HOTEL by ID (admin only)
    public delHotel ( id: string ): Observable<any> {

        console.log ( 'wywołano html request' , id )
        return this.http
            .post ( appConfig.apiUrl + '/api/hotel/del' + id , id , {
                headers: new HttpHeaders ().set ( 'Authorization' , this._authHeader )
            } )
            .catch ( this._handleError );
    }


  // GET all lotniska by Admin (admin only)
  public getLotniska (): Observable<any> {
      console.log('pobieram lotniska')
    return this.http
      .get ( appConfig.apiUrl + '/api/getLotniska'  )
      .catch ( this._handleError );
  }

    // VERYFI user by ID (admin only)
    public veryfiUser ( id: string ): Observable<any> {
        return this.http
            .post ( appConfig.apiUrl + '/api/users/veryfi' + id , id , {
                headers: new HttpHeaders ().set ( 'Authorization' , this._authHeader )
            } )
            .catch ( this._handleError );
    }

    // DEL user by ID (admin only)
    public delUser ( id: string ): Observable<any> {
        return this.http
            .post ( appConfig.apiUrl + '/api/users/del' + id , id , {
                headers: new HttpHeaders ().set ( 'Authorization' , this._authHeader )
            } )
            .catch ( this._handleError );
    }

    // GET user by ID (admin only)
    public getUserData ( id: string ): Observable<any> {
        return this.http
            .get ( appConfig.apiUrl + '/api/user/' + id , {
                                                              headers: new HttpHeaders ().set ( 'Authorization' , this._authHeader )
                                                          } )
            .catch ( this._handleError );
    }

    // GET all users (admin only)

    public get_Users (): Observable<any> {
        return this.http
            .get ( appConfig.apiUrl + '/api/users' )
            .catch ( this._handleError );
    }

    // UPDATE user by ID (admin only)
    public updateUser ( user: User ): Observable<User> {
        console.log ( user );
        return this.http
            .post ( appConfig.apiUrl + '/api/user/update' + user._id , user , {
                headers: new HttpHeaders ().set ( 'Authorization' , this._authHeader )
            } )
            .catch ( this._handleError );
    }

    // CREATE USER

    public createuser ( user ) {

        return this.http.post ( appConfig.apiUrl + '/api/createuser' , user );
    }


    // ZAMOWIENIA MODEL
    // GET all ZAMOWIENIA ACTIVE DISABLED (admin only)

    public get_orders_active_user (id): Observable<any> {

         return this.http
            .get ( appConfig.apiUrl + '/api/orders/active', id  )
            .catch ( this._handleError );
    }
    public get_orders_disabled_user (id): Observable<any> {

        return this.http
            .get ( appConfig.apiUrl + '/api/orders/disabled', id  )
            .catch ( this._handleError );
    }



    public get_all_zamowienia (): Observable<any> {
        return this.http
            .get ( appConfig.apiUrl + '/api/zamowienia' )
            .catch ( this._handleError );
    }

    // ADD ZAMOWIENIE (admin only)
    public add_Order ( order: ZamowieniaModel ): Observable<any> {
        return this.http
            .post ( appConfig.apiUrl + '/order/add' , order , {
                headers: new HttpHeaders ().set ( 'Authorization' , this._authHeader )
            } )
            .catch ( this._handleError );

    }

    //// TRASY MODEL
    //// GET list of public trasy
    // getTrasy$ (): Observable<any> {
    //    const temp = this.http
    //        .get ( appConfig.apiUrl + '/api/trasy' )
    //        .catch ( this._handleError )
    //    return temp;
    //}
    //
    //// POST new trasa (admin only)
    //postTrasa$ ( trasa: TrasyModel ): Observable<any> {
    //    return this.http
    //        .post ( appConfig.apiUrl + '/trasy/new' , trasa , {
    //            headers: new HttpHeaders ().set ( 'Authorization' , this._authHeader )
    //        } )
    //        .catch ( this._handleError );
    //}
    //
    //// PUT existing trasa (admin only)
    //editTrasa$ ( id: string , trasa: TrasyModel ): Observable<any> {
    //    return this.http
    //        .put ( appConfig.apiUrl + `/trasy/${id}` , trasa , {
    //            headers: new HttpHeaders ().set ( 'Authorization' , this._authHeader )
    //        } )
    //        .catch ( this._handleError );
    //}
    //
    //// GET all trasy - private and public (admin only)
    //getAdminTrasy$ (): Observable<any> {
    //    return this.http
    //        .get ( `${ENV.BASE_API}trasy/admin` , {
    //            headers: new HttpHeaders ().set ( 'Authorization' , this._authHeader )
    //        } )
    //        .catch ( this._handleError );
    //}
    //
    //// GET an event by ID (login required)
    //getTrasyById$ ( id: string ): Observable<any> {
    //    return this.http
    //        .get ( `${ENV.BASE_API}trasy/${id}` , {
    //            headers: new HttpHeaders ().set ( 'Authorization' , this._authHeader )
    //        } )
    //        .catch ( this._handleError );
    //}
    //
    //// END TRASY
    //
    //// END TRASY
    //
    //// GET list of public, future events
    //getEvents$ (): Observable<TrasyModel> {
    //    return this.http
    //        .get ( `${ENV.BASE_API}events` )
    //        .catch ( this._handleError );
    //}

    // GET all events - private and public (admin only)
    // getAdminEvents$(): Observable<EventModel[]> {
    //   return this.http
    //     .get(`${ENV.BASE_API}events/admin`, {
    //       headers: new HttpHeaders().set('Authorization', this._authHeader)
    //     })
    //     .catch(this._handleError);
    // }

    // // GET an event by ID (login required)
    // getEventById$(id: string): Observable<EventModel> {
    //   return this.http
    //     .get(`${ENV.BASE_API}event/${id}`, {
    //       headers: new HttpHeaders().set('Authorization', this._authHeader)
    //     })
    //     .catch(this._handleError);
    // }

    // // GET RSVPs by event ID (login required)
    // getRsvpsByEventId$(eventId: string): Observable<RsvpModel[]> {
    //   return this.http
    //     .get(`${ENV.BASE_API}event/${eventId}/rsvps`, {
    //       headers: new HttpHeaders().set('Authorization', this._authHeader)
    //     })
    //     .catch(this._handleError);
    // }

    // POST new event (admin only)
    // postEvent$(event: EventModel): Observable<EventModel> {
    //   return this.http
    //     .post(`${ENV.BASE_API}event/new`, event, {
    //       headers: new HttpHeaders().set('Authorization', this._authHeader)
    //     })
    //     .catch(this._handleError);
    // }

    // PUT existing event (admin only)
    // editEvent$(id: string, event: EventModel): Observable<EventModel> {
    //   return this.http
    //     .put(`${ENV.BASE_API}event/${id}`, event, {
    //       headers: new HttpHeaders().set('Authorization', this._authHeader)
    //     })
    //     .catch(this._handleError);
    // }

    // DELETE existing event and all associated RSVPs (admin only)
    // deleteEvent$(id: string): Observable<any> {
    //   return this.http
    //     .delete(`${ENV.BASE_API}event/${id}`, {
    //       headers: new HttpHeaders().set('Authorization', this._authHeader)
    //     })
    //     .catch(this._handleError);
    // }

    // GET all events a specific user has RSVPed to (login required)
    // getUserEvents$(userId: string): Observable<EventModel[]> {
    //   return this.http
    //     .get(`${ENV.BASE_API}events/${userId}`, {
    //       headers: new HttpHeaders().set('Authorization', this._authHeader)
    //     })
    //     .catch(this._handleError);
    // }

    // POST new RSVP (login required)
    // postRsvp$(rsvp: RsvpModel): Observable<RsvpModel> {
    //   return this.http
    //     .post(`${ENV.BASE_API}rsvp/new`, rsvp, {
    //       headers: new HttpHeaders().set('Authorization', this._authHeader)
    //     })
    //     .catch(this._handleError);
    // }

    // PUT existing RSVP (login required)
    // editRsvp$(id: string, rsvp: RsvpModel): Observable<RsvpModel> {
    //   return this.http
    //     .put(`${ENV.BASE_API}rsvp/${id}`, rsvp, {
    //       headers: new HttpHeaders().set('Authorization', this._authHeader)
    //     })
    //     .catch(this._handleError);
    // }

    private _handleError ( err: HttpErrorResponse | any ) {
        const errorMsg = err.message || 'Error: Unable to complete request.';
        if ( err.message && err.message.indexOf ( 'No JWT present' ) > -1 ) {
            // this.auth.login();
        }
        return Observable.throw ( errorMsg );
    }

}
