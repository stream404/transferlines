import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from './_guards/index';

import {TemporaryComponent} from './_layout/temporary/temporary.component';
import {SiteLayoutComponent} from './_layout/site-layout/site-layout.component';
import {AppLayoutComponent} from './_layout/app-layout/app-layout.component';


import {DashboardComponent} from './partners/dashboard/dashboard.component';
import {OrdersComponent} from './partners/dashboard/orders/orders.component';
import {AddUsersComponent} from './partners/dashboard/users/add.component';
import {UserProfileComponent} from './partners/user-profile/user-profile.component';
import {LoginComponent} from './partners/login/login.component';
import {AddLotComponent} from './partners/add-lot/add-lot.component';
import {ListComponent} from './partners/dashboard/users/list.component';
import {HoteleComponent} from './partners/dashboard/hotele/hotele.component';
import {RegisterComponent} from './partners/register/register.component';

import {WelcomeComponent} from './client/welcome/welcome.component';
import {HomeComponent} from './client/home/home.component';


const routes: Routes = [
    {
      path: '',
      component: TemporaryComponent,
      children: [
        {path: '', component: TemporaryComponent}
      ]
    },

    {
      path: '',
      component: SiteLayoutComponent,
      children: [
        {path: '', component: HomeComponent, canActivate: [AuthGuard]},
        {path: 'app', component: WelcomeComponent, pathMatch: 'full'},
        {path: 'about', component: WelcomeComponent}
      ]
    },

    // App routes goes here here
    {
      path: 'app',
      component: AppLayoutComponent,
      children: [
        {path: '', component: DashboardComponent, canActivate: [AuthGuard]},
        {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
        {path: 'dashboard/user-profile', component: UserProfileComponent},
        {path: 'dashboard/ad-wycieczki', component: AddLotComponent},
        {path: 'dashboard/users/list', component: ListComponent},
        {path: 'dashboard/users/add', component: AddUsersComponent},
        {path: 'dashboard/hotele', component: HoteleComponent},
        {
          path: 'dashboard/zamowienia', component: OrdersComponent, data: [{display: 'active'}]
        },
        {
          path: 'dashboard/zamowienia/aktywne', component: OrdersComponent, data: [{display: 'active'}]
        },
        {
          path: 'dashboard/zamowienia/wygasle', component: OrdersComponent, data: [{display: 'disabled'}]
        },
        {
          path: 'dashboard/zamowienia/add', component: AddLotComponent
        }

      ]
    },

// no layout routes

    {
      path: 'login', component:
      LoginComponent
    }
    ,
    {
      path: 'dodajtransfer', component:
      AddLotComponent
    }
    ,
    {
      path: 'register', component:
      RegisterComponent
    }
    ,

// otherwise redirect to home

    {
      path: '**', redirectTo:
        ''
    }
  ]
;


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [],
})
export class AppRoutingModule {
}

