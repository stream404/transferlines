import { Component, OnInit } from '@angular/core';
import {User} from '../../_models/index';
import {UserService} from '../../_services/index';
import {ApiService} from '../../_services/api.service';
import { appConfig } from '../../app.config';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
    currentUser: User;
    fileToUpload: File = null;
    public currentUserAjax;
    roleAdmin = [{
        'id': 'admin',
        'name': 'Administrator'
    }, {
        'id': 'user',
        'name': 'Użytkownik'
    }];
    constructor(private userService: UserService, public apiService: ApiService) {
        this.currentUser = userService.currentUser();
        this.currentUserAjax = apiService.getUserData(this.currentUser._id).subscribe(next => {
          // console.log(next['email']);
            this.currentUser.email = next['email'];
            this.currentUser.username = next['username'];
            this.currentUser.password = next['password'];
            this.currentUser.firstName = next['firstName'];
            this.currentUser.lastName = next['lastName'];
            this.currentUser.role = next['role'];
            this.currentUser.hotelID = next['hotelID'];
            this.currentUser.image = next['image'];
        });
    }

    log() {
        $('#submit').click();
        console.log(localStorage);
    }
    userUpdateForm(form) {

        form.value._id = this.currentUser._id;
        // form.value.role = this.currentUser.role;
        form.value.image = this.currentUser.image;
        this.apiService.updateUser(form.value).subscribe(next => {
            // console.log(next);
            localStorage.setItem( 'currentUser', JSON.stringify(form.value) );

        });
    }
    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
        // console.log('fileSelected to upload  : ' + this.fileToUpload);
       this.uploadFileToActivity();
    }
    uploadFileToActivity() {
        // console.log(this.currentUser.username);
        this.apiService.postFile(this.fileToUpload, this.currentUser.username).subscribe(data => {
            // do something, if upload success
            this.currentUser.image = appConfig.serverUrl + 'server/upload/' + this.fileToUpload.name;
            $('#submit').click();
        }, error => {
            // console.log(error);
        });
    }
  ngOnInit() {
  }

}
