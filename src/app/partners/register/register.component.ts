﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService, UserService, AuthenticationService } from '../../_services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'register.component.html'
})

export class RegisterComponent {
    model: any = {};
    loading = false;

    constructor(
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private authenticationService: AuthenticationService) { }

        routerTest() {
            this.alertService.error('erooroor', false);
    }
    register() {
        this.alertService.success('Wysyłanie', false);
        this.loading = true;
        this.userService.create(this.model)
            .subscribe(
                data => {
                    this.alertService.success('Zarejestrowano poprawnie', true);

                    this.authenticationService.login(this.model.username, this.model.password)
                        .subscribe(
                            user => {
                                console.log('navigate to user profile' +  user);

                                this.router.navigate(['/user-profile']);
                            },
                            error => {
                                this.alertService.error(error);
                                this.loading = false;
                            });
                    // this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error, false);
                    this.loading = false;
                });
    }
}
