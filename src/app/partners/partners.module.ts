import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, NgForm} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import {AlertService} from '../_services';
import {LoginComponent} from './login/login.component';
import {ListComponent} from './dashboard/users/list.component';
import {AddLotModule} from './add-lot/add-lot.module';
import {EditUsersComponent} from './dashboard/users/edit.component';
import {HoteleComponent} from './dashboard/hotele/hotele.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {OrdersComponent} from './dashboard/orders/orders.component';
import {AddUsersComponent} from './dashboard/users/add.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {RegisterComponent} from './register/register.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AddLotModule,
    RouterModule,
    ConfirmationPopoverModule,
  ],
  declarations: [
    LoginComponent,
    ListComponent,
    EditUsersComponent,
    HoteleComponent,
    DashboardComponent,
    OrdersComponent,
    AddUsersComponent,
    UserProfileComponent,
    RegisterComponent,


  ],
  providers: [AlertService],
  exports: []
})
export class PartnersModule {
}
