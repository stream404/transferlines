import {Component, OnInit} from '@angular/core';
import {AlertComponent} from '../../../_layout/app-components/alert/index';
import {ZamowieniaModel} from '../../../_models/zamowienia';
import {AlertService, UserService} from '../../../_services';
import {AddLotFormService} from './add-lot-form.service';
import {ApiService} from '../../../_services/api.service';
import {FormGroup} from '@angular/forms';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-add-lot-form',
  templateUrl: './add-lot-form.component.html',
  styleUrls: ['./add-lot-form.component.scss'],
  providers: [
    AddLotFormService,
    ApiService,
    AlertComponent
  ]
})
export class AddLotFormComponent implements OnInit {

  public currentUser = this.userService.currentUser();
  public myForm: FormGroup;
  public element: object;
  public hotelVisible: boolean;
  public listaHoteli = [];
  public userHotel = []
  submitted = false;

  lotniska = [];
  directionOpis = [{
    'id': 1,
    'name': 'Odbiór z lotniska'
  },
    {
      'id': 2,
      'name': 'Transfer na lotnisko'
    }];

  model = new ZamowieniaModel(
    'lot',
    'start',
    new Date(),
    new Date(),
    new Date(),
    new Date(),
    'location_IN',
    'location_IN',
    0,
    'usernameRecepcja',
    'event_registred_name',
    'event_registred_lastName',
    'event_registred_payStatus',
    '',
  );

  constructor(private alertService: AlertService,
              private Addservice: AddLotFormService,
              private apiService_: ApiService,
              private userService: UserService) {
  }





  selectDirection(t) {
    console.log(t + 'Setting up visibility  ');
    if (t === '2') {
      this.hotelVisible = true;
    } else {
      this.hotelVisible = false;
    }
    console.log(this.myForm);

  }

  get diagnostic() {
    return JSON.stringify(this.model);
  }


  addOrder(myForm) {


    this.alertService.modal('Wysyłanie', false);

    this.model.date_add = new Date();
    this.model.date_lastChange = this.model.date_add;
    this.model.distance = 120;
    this.model.event_date = myForm.event_date;
    this.model.event_time = myForm.event_time;
    this.model.event_registred_name = myForm.event_registred_name;
    this.model.event_registred_lastName = myForm.event_registred_lastName;
    this.model.event_registred_payStatus = 'unknown';
    this.model.location_IN = '';
    this.model.location_OUT = '';
    this.model.state = 'start';
    this.model.type = 'lot'

    // const tem = this.apiService_.add_Order ( this.model ).subscribe ( doc => {
    //     if ( doc ) {
    //         console.log ( 'zapisano poprawnie' );
    //         this.alertService.success ( 'Zapisano poprawnie' , false );
    //     }
    // } )




    console.log('Registration successful.');
    console.log(myForm);
    console.log(this.model)
  }

  ngOnInit() {

    this.hotelVisible = false;

    const temp = this.apiService_.get_Hotele().subscribe(doc => {
      if (doc) {
        this.listaHoteli = doc

        console.log(this.listaHoteli);
      }


    })

    const tem = this.apiService_.getLotniska().subscribe(doc => {
      if (doc) {
        doc.forEach(el => {
          this.lotniska.push(el);
        })
      }
    })


    this.myForm = new FormGroup({
      'direction': new FormControl(),
      'name': new FormControl(),
      'hotel': new FormControl(),
      'lotnisko': new FormControl(),
      'personalInfo': new FormControl(),
      'personalCount': new FormControl(),
      'lotNumber': new FormControl()
    });
  }


}
