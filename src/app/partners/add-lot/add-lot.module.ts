import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddLotComponent} from './add-lot.component';
import {AddLotFormComponent} from './add-lot-form/add-lot-form.component';
import {AddLotFormService} from './add-lot-form/add-lot-form.service';

@NgModule({
  imports: [
    CommonModule
  ],
    declarations: [
        AddLotComponent,
        AddLotFormComponent
    ],
    providers: [
        AddLotFormService
    ]
})
export class AddLotModule { }
