import {Component, HostListener, OnInit} from '@angular/core';
import {ApiService} from '../../../_services/api.service';
import {AlertService, UserService} from '../../../_services';
import {ActivatedRoute} from '@angular/router';
import {_} from 'underscore';


declare var jquery: any;
declare var $: any;


@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})

export class OrdersComponent implements OnInit {
  public hotele = [];
  public wizard = 'hidden';
  public ZamowienieInfo = [];
  public hotelUsers = [];
  public fileToUpload = [];
  public loading = false;


  public orders = null;

  public displayZamowienia = '';
  public zamowienia = [];
  public zamowienieINFO
  public currentUser = this.userService.currentUser()

  constructor(private route: ActivatedRoute,
              private alertService: AlertService,
              private userService: UserService,
              private apiService: ApiService) {
  }

  @HostListener('window:keyup', ['$event'])

  keyEvent(event: KeyboardEvent) {
    if (event.key === 'Escape') {
      this.wizard = 'hidden';
    }
  }

  delZamowienie() {
  }

  sortTable(el, desc) {
    let temp = [];
    if (el === 'location_IN') {
      temp = _.sortBy(this.zamowienia, 'location_IN')
    }
    if (el === '_id') {
      temp = _.sortBy(this.zamowienia, 'location_IN')
    }
    if (desc) {
      this.zamowienia = temp.reverse()
    } else {
      this.zamowienia = temp
    }
  }

  showZamowienie(ID) {
    this.wizard = 'visible';
    this.zamowienia.forEach(doc => {
      if (doc.ID === ID) {
        this.zamowienieINFO.push(doc);
      }
    });
  }

  switchingTabs() {
    $('.nav-pills li a').click(function () {
      $(this).toggleClass('active');
    })
  }

  ngOnInit() {
    this.switchingTabs();
    this.route.data.subscribe((value => {
      this.displayZamowienia = value[0].display;

      if (value[0].display === 'active') {
        const temp = this.apiService.get_orders_active_user(this.currentUser._id).subscribe(doc => {
          if (doc) {
            console.log(doc, 'zamowienia z bazy');
            doc.forEach(zamowienie => {
              this.zamowienia.push(zamowienie);
            })
          }
        });

      }
      if (value[0].display === 'disabled') {
        const temp = this.apiService.get_orders_disabled_user(this.currentUser._id).subscribe(doc => {
          if (doc) {
            console.log(doc, 'zamowienia z bazy');
            doc.forEach(zamowienie => {
              this.zamowienia.push(zamowienie);
            })
          }
        });

      }
    }));


  }

}
