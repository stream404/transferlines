import { Component , OnInit } from '@angular/core';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { UserService } from '../../../_services/index';
import { User } from '../../../_models/index';
import { ApiService } from '../../../_services/api.service';
import {Router} from '@angular/router';

@Component ( {
                 selector: 'app-list' ,
                 templateUrl: './list.component.html' ,
                 styleUrls: [ './list.component.scss' ] ,
                 providers: [ ConfirmationPopoverModule ]
             } )
export class ListComponent implements OnInit {
    usersVerified: User[] = [];
    usersNotVerified: User[] = [];
    rout = Router;
    public alert;

    constructor ( private  userService: UserService , public apiUsers: ApiService ) { }

    private delUser ( user_id , element ) {
        const del = this.apiUsers.delUser ( user_id ).subscribe ( doc1 => {
            console.log ( doc1 );
            this.usersVerified = [];
            this.usersNotVerified = [];
            this.loadAllUsers ();
        } )
    }

    private verifyUser ( user_id , element ) {
        const veryfi = this.apiUsers.veryfiUser ( user_id ).subscribe ( doc1 => {
            console.log ( doc1 );
            this.usersVerified = [];
            this.usersNotVerified = [];
            this.loadAllUsers ();
        } )
    }


    private loadAllUsers () {
        const validuser = [];
        const tes = this.apiUsers.get_Users ().subscribe ( users => {
            const usersArray = [];
            users.forEach ( user => {
                if ( user.valid === 'true' ) {
                    this.usersVerified.push ( user );
                } else {
                    this.usersNotVerified.push ( user );
                }
            } );
        } );

        const usersXX = this.apiUsers.get_Users ();

    }

    ngOnInit () {
        this.loadAllUsers ();

    }

}
