import { Component , OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../../../_services/api.service';
import { AlertService , UserService } from '../../../_services';
import { FormGroup , FormControl , Validators } from '@angular/forms';
import { Location } from '@angular/common';

@Component ( {
                 selector: 'app-users-add' ,
                 templateUrl: './add.component.html' ,
                 styleUrls: [ './add.component.scss' ] ,
                 providers: [ NgForm ]
             } )
export class AddUsersComponent implements OnInit {
    currentUser = this.userService.currentUser ();
    userAddModel: any = {};
    hoteleAdmina = [];
    tempUserArray: any = {};
    roleAdmin = [ {
        'id': 'admin' ,
        'name': 'Administrator'
    } , {
        'id': 'user' ,
        'name': 'Użytkownik'
    } ];

    constructor (
        public apiService: ApiService ,
        private userService: UserService ,
        private alertService: AlertService ,
        private router: Router ,
        private _location: Location
    ) { }

    back () {

        this._location.back()

    }

    ngOnInit () {
        this.getHotele ();

        this.userAddModel =
            new FormGroup ( {
                                'username': new FormControl ( this.userAddModel.username , [
                                    Validators.required ,
                                    Validators.minLength ( 6 )
                                ] ) ,
                                'password': new FormControl ( this.userAddModel.password , [
                                    Validators.required ,
                                    Validators.minLength ( 6 )
                                ] ) ,
                                'firstName': new FormControl ( this.userAddModel.firstName , [
                                    Validators.required ,
                                    Validators.minLength ( 3 )
                                ] ) ,
                                'lastName': new FormControl ( this.userAddModel.lastName , [
                                    Validators.required ,
                                    Validators.minLength ( 3 )
                                ] ) ,
                                'hotelID': new FormControl ( this.userAddModel.hotelID , [ Validators.required ] ) ,
                                'email': new FormControl ( this.userAddModel.email , [ Validators.required , Validators.email ] )
                            } );
    }


    addUser ( form ) { // DODANIE UŻYTKOWNIKA Z FORMULARZA
        if ( this.currentUser.role === 'admin' ) {

            if ( form.invalid ) {
                this.alertService.error ( 'popraw błędy' , false )
            } else {
                this.alertService.hide ( 'poprawiono' );
                this.tempUserArray = form.value;
                this.tempUserArray.image = '/assets/img/dumny_user.png';
                this.tempUserArray.valid = 'true';
                this.tempUserArray.role = 'user';
                this.tempUserArray.verifiedBy = this.currentUser._id;

                console.log ( this.tempUserArray );
                this.apiService.createuser ( this.tempUserArray )
                    .subscribe (
                        data => {
                            console.log ( data );
                            this.alertService.success ( 'Zarejestrowano poprawnie' , true );
                            this.router.navigate ( [ '/app/dashboard' ] );
                        } ,
                        error => {
                            console.log ( error );
                            this.alertService.error ( error , false );
                            // this.loading = false;
                        }
                    );

            }


        }

    }

    getHotele () { // POBRANIE HOTELI DANEGO ADMINA
        if ( this.currentUser.role === 'admin' ) {
            console.log ( this.currentUser._id.toString () )
            const temp = this.apiService.get_AdminsHotele ( this.currentUser._id.toString () ).subscribe (
                d => {
                    d.forEach ( e => {
                        this.hoteleAdmina.push ( e )
                    } )
                } ,
                e => {
                    this.alertService.error ( 'wystapił błąd podczas pobierania danych o recepcjach:<br> ' + e , false )
                } )
        }
    }


}
