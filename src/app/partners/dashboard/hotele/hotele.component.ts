import { Component , OnInit } from '@angular/core';
import { ApiService } from '../../../_services/api.service';
import { AlertService , UserService } from '../../../_services';
import { appConfig } from '../../../app.config';

declare var jquery: any;
declare var $: any;

@Component ( {
                 selector: 'app-hotele' ,
                 templateUrl: './hotele.component.html' ,
                 styleUrls: [ './hotele.component.css' ]
             } )

export class HoteleComponent implements OnInit {
    public hotele = [];
    public wizard = 'hidden';
    public HotelInfo = [];
    public hotelUsers = [];
    public fileToUpload = [];
    public loading = false;

    constructor (
        private alertService: AlertService ,
        private userService: UserService ,
        private apiService: ApiService

    ) { }


    getHotele () {
        const temp = this.apiService.get_Hotele ().subscribe ( hotele => {
            // console.log(hotele)
            hotele.forEach ( hotel => {
                this.hotele.push ( hotel );
            } )
        } );
    }

    loadAllUsers ( hotelId ) {
        const tes = this.apiService.get_Users ().subscribe ( users => {
            users.forEach ( user => {
                // console.log(user.hotelID, hotelId)
                if ( user._id === hotelId ) {
                    // console.log(true);
                    // console.log(user.hotelID, hotelId)
                    this.hotelUsers.push ( user );
                }
            } );
        } );

    }

    showHotelWizzard ( hotelId ) {
        this.wizard = 'visible';
        this.hotele.forEach ( hotel => {
            // console.log(hotel.hotelID);
            if ( hotel._id === hotelId ) {
                this.HotelInfo.push ( hotel );
            }
            this.loadAllUsers ( hotelId );
        } );
        // console.log(this.HotelInfo);
    }

    zapiszHotelInfo ( form ) {

        this.loading = true;
        //  this.alertService.success('Wysyłanie', false);
        this.alertService.modal ( 'Wysyłanie' , false );
        const image = $ ( '#wizardPicturePreview' ).attr ( 'src' );
        form.value.image = image
        // console.log('pobralem form :' , form.value)
        // this.wizard = 'hidden';
        //   this.HotelInfo = [];
        this.apiService.save_Hotel ( form.value ).subscribe ( next => {
            // console.log(next);

            if ( next.location ) {
                this.HotelInfo[ 0 ] = next
                this.alertService.hide ( 'hidden' );
                const temp = [];
                this.hotele.forEach ( doc => {

                    console.log ( next._id );
                    console.log ( doc._id );

                    if ( doc._id === next._id ) {
                        doc.name = next.name;
                        doc.location = next.location;
                        doc.image = next.image
                    }
                    temp.push ( doc );
                } )

            }

            this.wizard = 'hidden';
            this.HotelInfo = [];
        } );
    }

    switchingTabs () {
        $ ( '.nav-pills li a' ).click ( function () {
            $ ( this ).toggleClass ( 'active' );
        } )
    }

    handleFileInput ( files: FileList , hotelId ) {

        this.apiService.postAvatarHotel ( files.item ( 0 ) , hotelId ).subscribe ( data => {
            const temp = [];
            if ( data === true ) {

                console.log ( 'image upload true: ' + appConfig.serverUrl + 'server/upload/' + files.item ( 0 ).name )
                this.hotele.forEach ( doc => {
                    if ( doc._id === hotelId ) {
                        doc.image = appConfig.serverUrl + 'server/upload/' + files.item ( 0 ).name;
                    }
                    temp.push ( doc );
                } )
            }
        } , error => {
            // console.log(error);
        } );
    }

    delHotel ( hotelId ) {

        console.log ( hotelId );

        const deleted = this.apiService.delHotel ( hotelId ).subscribe ( obj => {

            if ( obj ) {

                console.log('zapisano')


            }

        } )


    }

    ngOnInit () {
        this.getHotele ();
        this.switchingTabs ();
    }

}
