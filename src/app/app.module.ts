import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


import {HttpClientModule, HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {APP_BASE_HREF, DatePipe} from '@angular/common';

import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app.routing';
import {ComponentsModule} from './_layout/app-components/components.module';
import {AppComponent} from './app.component';
import {AlertComponent} from './_layout/app-components/alert/index';
import {AuthGuard} from './_guards/index';
import {JwtInterceptorProvider, ErrorInterceptorProvider} from './_helpers/index';
import {AlertService, AuthenticationService, GlobalApp, UserService} from './_services/index';
import {ApiService} from './_services/api.service';
import {SiteLayoutComponent} from './_layout/site-layout/site-layout.component';
import {SiteFooterComponent} from './_layout/site-footer/site-footer.component';
import {SiteHeaderComponent} from './_layout/site-header/site-header.component';
import {AppLayoutComponent} from './_layout/app-layout/app-layout.component';
import {AppHeaderComponent} from './_layout/app-header/app-header.component';
import {HttpModule} from '@angular/http';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import {TemporaryComponent} from './_layout/temporary/temporary.component';
import {HomeComponent} from './client/home/home.component';
import {WelcomeComponent} from './client/welcome/welcome.component';
import {PartnersModule} from './partners/partners.module';
import {ClientModule} from './client/client.module';
import { AgmCoreModule } from '@agm/core';


@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    SiteLayoutComponent,
    SiteFooterComponent,
    SiteHeaderComponent,
    AppLayoutComponent,
    AppHeaderComponent,
    TemporaryComponent,
    WelcomeComponent,
    HomeComponent,

  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC0r73aOtyocAd579xx2Zdlrs8cKyVMp5E'
      // apiKey: 'AIzaSyA4TJKt7d5FFAqrXVUYOQLiRfyIu46gTtU'
      // apiKey: 'AIzaSyC3m2q6nraPUnoeiiTVhTr2q5YMzfBjBT4'
    }),
    ClientModule,
    PartnersModule,
    BrowserModule,
    FormsModule,

    HttpClientModule,
    ComponentsModule,
    RouterModule,
    HttpModule,
    AppRoutingModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger'

      // set defaults here
    })
  ],
  providers: [
    ApiService,
    AuthGuard,
    AlertService,
    AuthenticationService,
    UserService,
    JwtInterceptorProvider,
    ErrorInterceptorProvider,
    ReactiveFormsModule,
    FormsModule,
    GlobalApp,
    ConfirmationPopoverModule,
    {provide: APP_BASE_HREF, useValue: '/'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

