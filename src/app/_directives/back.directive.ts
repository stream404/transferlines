import { Directive , ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { HostListener } from '@angular/core';

@Directive ( {
                 selector: '[appMyClick]' ,
             } )
export class BackDirective {
    @Output () appMyClick: EventEmitter<any> = new EventEmitter ();

    @HostListener ( 'click' , [ '$event' ] )
    onClick ( e ) {
        if ( e ) {
            e.preventDefault ();
            e.stopPropagation ();
        } else {
            this.appMyClick.next ( e );
        }
    }
}
