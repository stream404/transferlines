import { Component , OnInit , ViewChild , AfterViewInit } from '@angular/core';
import { Location , LocationStrategy , PathLocationStrategy , PopStateEvent } from '@angular/common';
import 'rxjs/add/operator/filter';
import { NavbarComponent } from './_layout/app-components/navbar/navbar.component';
import { Router , NavigationEnd , NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import PerfectScrollbar from 'perfect-scrollbar';
import { UserService } from './_services';


declare var jquery: any;
declare var $: any;

@Component ({
    selector: 'app-root' ,
    templateUrl: './_layout/app.component.html' ,
    styleUrls: [ './_layout/app.component.css' ]
})


export class AppComponent implements OnInit {
    public currentUser = this.userService.currentUser();
    private _router: Subscription;
    private lastPoppedUrl: string;
    private yScrollStack: number[] = [];
    @ViewChild (NavbarComponent) navbar: NavbarComponent;

    constructor (public location: Location , private router: Router , public userService: UserService) {
        // router.events.subscribe((val) => {
        //     console.log();
        //     if ( location.path() !== '') {
        //         this.route = location.path();
        //     } else {
        //         this.route = 'Home'
        //     }
        // });
    }
    ngOnInit () {
        $.material.init ();
        this.localUser ();
        const elemMainPanel = <HTMLElement>document.querySelector ('.main-panel');
        const elemSidebar = <HTMLElement>document.querySelector ('.sidebar .sidebar-wrapper');
        this.location.subscribe ((ev: PopStateEvent) => {
            this.lastPoppedUrl = ev.url;
        });
        this.router.events.subscribe ((event: any) => {
            // this.navbar.sidebarClose();
            if ( event instanceof NavigationStart ) {
                if ( event.url !== this.lastPoppedUrl ) {
                    this.yScrollStack.push (window.scrollY);
                }
            } else if ( event instanceof NavigationEnd ) {
                if ( event.url === this.lastPoppedUrl ) {
                    this.lastPoppedUrl = undefined;
                    window.scrollTo (0 , this.yScrollStack.pop ());
                } else {
                    window.scrollTo (0 , 0);
                }
            }
        });
        this._router = this.router.events.filter (event => event instanceof NavigationEnd).subscribe ((event: NavigationEnd) => {
            // elemMainPanel.scrollTop = 0;
            // elemSidebar.scrollTop = 0;
        });
        if ( window.matchMedia (`(min-width: 960px)`).matches && !this.isMac () ) {
            let ps = new PerfectScrollbar (elemMainPanel);
            ps = new PerfectScrollbar (elemSidebar);
        }
    }
    ngAfterInit () {
        this.runOnRouteChange ();
    }
    isMaps (path) {
        let titlee = this.location.prepareExternalUrl (this.location.path ());
        titlee = titlee.slice (1);
        if ( path === titlee ) {
            return false;
        } else {
            return true;
        }
    }
    localUser () {
        this.currentUser = this.userService.currentUser ();
        return this.currentUser
    }
    runOnRouteChange (): void {
        if ( window.matchMedia (`(min-width: 960px)`).matches && !this.isMac () ) {
            const elemMainPanel = <HTMLElement>document.querySelector ('.main-panel');
            const ps = new PerfectScrollbar (elemMainPanel);
            ps.update ();
        }
    }
    isMac (): boolean {
        let bool = false;
        if ( navigator.platform.toUpperCase ().indexOf ('MAC') >= 0 || navigator.platform.toUpperCase ().indexOf ('IPAD') >= 0 ) {
            bool = true;
        }
        return bool;
    }
}
